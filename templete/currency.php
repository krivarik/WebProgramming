
<br><br><br>
<br>
<div class="container">

    <div class="starter-template">
        <form  method='POST'>
            <h4>Выберите базовую валюту</h4><br>
            <select name="base_currency">
                <option value="NUL">Выбрать базовую валюту
                <option value="RUB">Рубль
                <option value="USD">Долар
                <option value="EUR">Евро
            </select><br><br>
            <h2>Выберите с какими валютами сравнить</h2>
            <input type="checkbox" name="currency[]" value="RUB">Рубль
            <input type="checkbox" name="currency[]" value="USD"> Долар
            <input type="checkbox" name="currency[]" value="EUR"> Евро
            <br><br>
            <input type="submit"  value="Отправить">
            <br>
        </form>
    </div>
    <br><br>
<?php
if (!empty($_POST['base_currency']) && !empty($_POST['currency'])){
    $arrayCurrency=[
        'base'=>$_POST['base_currency'],
        'symbols'=>implode(',',$_POST['currency'])
    ];
    $fixerIo="http://api.fixer.io/latest?";
    $fixerIo.=http_build_query($arrayCurrency);
    $rates=json_decode(file_get_contents($fixerIo), true);
    echo "Базовая валюта: {$rates['base']} <br>";
    echo "Дата курса валют: {$rates['date']} <br>";
    echo "Курс за 1 {$rates['base']}: <br>";
    foreach ($rates['rates'] as $key=>$value){
        echo "{$key}={$value} <br>";
    }
}else{
    echo "Не выбрана базовая валюта или сравнивываемые валюты";
}



?>