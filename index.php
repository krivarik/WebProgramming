<?php
require_once __DIR__."/templete/header.html";
if (empty($_GET['page'])){
    require_once __DIR__."/templete/home.php";
}
elseif(file_exists(__DIR__."/templete/{$_GET['page']}")){
    
    require_once __DIR__."/templete/{$_GET['page']}";
}
else {
    require_once __DIR__."/templete/404.php";
}
require_once __DIR__."/templete/footer.html";